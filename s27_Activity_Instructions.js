// Activity s27

// 1. Create sample documents following the models we have created for users and courses for our booking-system.

	// - user1 and user2 are the ids for the user documents.
	// - course1 and course2 are the ids for the course documents.

	// - user1 is enrolled in course1.
	// - user2 is enrolled in course2.

// 2 Model Booking System with Embedding

user1 {

	"id": user1,
	"username": user1 ,
	"firstName": user,
	"lastName": juan,
	"email": userjuan@gmail.com,
	"password": userjuan,
	"mobileNumber": 65188481,
	"isAdmin": false,
	"enrollments": [
		{

			"id" - user1,
			"courseId" - course1,
			"courseName" - secretCourse,
			"isPaid" - false,
			"dateEnrolled": 010122
		}
	]

}


course {

	id - unique for the document
	name,
	description,
	price,
	slots,
	schedule,
	instructor,
	isActive,
	enrollees: [

		{
			id - document identifier,
			userId,
			userName(optional),
			isPaid,
			dateEnrolled
		}

	]

}


//3 Model Booking System with Referencing

user {

	id - unique identifier for the document,
	username,
	firstName,
	lastName,
	email,
	password,
	mobileNumber,
	isAdmin

}


course {

	id - unique for the document
	name,
	description,
	price,
	slots,
	schedule,
	instructor,
	isActive

}

enrollment:
{
	id - document identifier,
	userId,
	userName(optional),
	courseId,
	courseName(optional),
	isPaid,
	dateEnrolled
}


// 3. Create a new repo called s27

//4. Initialize your local repo, add and commit with the following message: "Add Activity Code"

// 5. Push and link to boodle (WDC028v1.5b-27 | MongoDB - Data Modeling and Translation).
